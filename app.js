new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns: []
  },
  methods: {
    attack: function() {
      this.$refs.attack.play();
      var damage = this.calculateDamage(3, 10);
      this.monsterHealth -= damage;
      this.turns.unshift({
        isPlayer: true,
        text: `Infringes ${damage} de daño al Monstruo!`
      });
      if (this.checkWin()) {
        return;
      }
      this.monsterAttack();
    },
    specialAttack: function() {
      this.$refs.specialAttack.play();
      var damage = this.calculateDamage(4, 15);
      this.monsterHealth -= damage;
      this.turns.unshift({
        isPlayer: true,
        text: `Infringes ${damage} de daño al Monstruo con Ataque Especial`
      });
      if (this.checkWin()) {
        return;
      }
      this.monsterAttack();
    },
    monsterAttack: function() {
      var damage = this.calculateDamage(5, 12);
      this.playerHealth -= damage;
      this.turns.unshift({
        isPlayer: false,
        text: `El monstruo te infringe ${damage} de daño !`
      });
      this.checkWin();
    },
    heal: function() {
      this.$refs.heal.play();
      if (this.playerHealth <= 90) {
        this.playerHealth += 10;
      } else {
        this.playerHealth = 100;
      }
      this.turns.unshift({
        isPlayer: true,
        text: `Recuperas 10 de Salud!`
      });
      this.monsterAttack();
    },
    startNewGame: function() {
      this.playerHealth = 100;
      this.monsterHealth = 100;
      this.gameIsRunning = true;
      this.turns = [];
    },
    giveUp: function() {
      this.gameIsRunning = false;
    },
    calculateDamage: function(min, max) {
      var damage = Math.max(Math.floor(Math.random() * max + 1), min);
      return damage;
    },
    checkWin: function() {
      if (this.monsterHealth <= 0) {
        this.monsterHealth = 0;
        Swal.fire({
          type: 'success',
          title: 'Ganaste!!',
          showCancelButton: true,
          confirmButtonText: '¿Nuevo Juego?'
        }).then(result => {
          if (result.value) {
            this.startNewGame();
          }
        });
        this.gameIsRunning = false;
        this.$refs.youWin.play();
        return true;
      } else if (this.playerHealth <= 0) {
        this.playerHealth = 0;
        Swal.fire({
          type: 'error',
          title: 'Perdiste!!',
          showCancelButton: true,
          confirmButtonText: 'Nuevo Juego?'
        }).then(result => {
          if (result.value) {
            this.startNewGame();
          }
        });
        this.gameIsRunning = false;
        this.$refs.youLose.play();
        return true;
      }
      return false;
    }
  }
});
